- (void)loadView
{
    UIView *aView = [[[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds] autorelease];
    aView.backgroundColor = [UIColor whiteColor];
    self.view = aView;
    [aView release];
    _imageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 300, 400)];
    _imageView.userInteractionEnabled=YES;
    _imageView.image=[UIImage imageNamed:@"h1.jpeg"];
    [self.view addSubview:_imageView];
    [_imageView release];
 
    //长按手势
    UILongPressGestureRecognizer *longGnizer=nil;
    longGnizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self
                                                            action:@selector(longGo:)];
    [self.view addGestureRecognizer:longGnizer];
    [longGnizer release];
}
 
-(void)longGo:(UILongPressGestureRecognizer *)aGer{
    if (aGer.state==UIGestureRecognizerStateBegan) {
        NSLog(@"%s",__func__);
    }
     
}